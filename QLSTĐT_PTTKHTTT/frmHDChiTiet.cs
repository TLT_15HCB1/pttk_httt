﻿using QLSTĐT_PTTKHTTT.Xử_lý;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLSTĐT_PTTKHTTT
{
    public partial class frmHDChiTiet : Form
    {
        private int maHD;
        private int maCoupon;
        private int TienGiam;
        private int TienHang;
        private Coupon cp;
        private frmSoLuong ChildFrm;

        public frmHDChiTiet()
        {
            InitializeComponent();
        }

        public frmHDChiTiet(int maHD)
        {
            this.maHD = maHD;
            InitializeComponent();
        }

        private void frmHDChiTiet_Load(object sender, EventArgs e)
        {
            dgv.DataSource = Xử_lý.ChiTietHoaDonBUS.LayDSChiTietHoaDon(maHD);
            TienHang = Xử_lý.HoaDonBUS.TinhTienHang(maHD).GetValueOrDefault();
            lbTienHang.Text = TienHang.ToString("N0");
            TinhTienGiamGia();
        }

        private void btnCoupon_Click(object sender, EventArgs e)
        {
            if (btnCoupon.Text == "Áp dụng")
            {
                cp = Xử_lý.CouponBUS.TimCoupon((int)numMaCoupon.Value);
                if (cp == null)
                    MessageBox.Show("Mã Coupon không tồn tại, hãy nhập mã Coupon khác");
                else if (cp.NgayBatDau > DateTime.Now || cp.NgayKetThuc < DateTime.Now)
                {
                    MessageBox.Show("Coupon chưa đến thời hạn hoặc đã quá hạn sử dụng");
                    cp = null;
                }
                else
                {
                    MessageBox.Show("Đã kích hoạt Coupon '" + cp.TenCoupon + "'");
                    numMaCoupon.Enabled = false;
                    maCoupon = (int)numMaCoupon.Value;
                    btnCoupon.Text = "Đổi Coupon";
                    TinhTienGiamGia();
                }
            }
            else if (btnCoupon.Text == "Đổi Coupon")
            {
                maCoupon = 0;
                numMaCoupon.Enabled = true;
                btnCoupon.Text = "Áp dụng";
                TinhTienGiamGia();
            }
        }

        private void TinhTienGiamGia()
        {
            cp = Xử_lý.CouponBUS.TimCoupon(maCoupon);
            HoaDon hd = Xử_lý.HoaDonBUS.TimHoaDon(maHD);
            if (cp != null)
                TienGiam = ((TienHang * hd.TiLeGiamGia / 100) + (TienHang * cp.TiLeGiamGia / 100 + cp.SoTienDuocGiam)).GetValueOrDefault();
            else
                TienGiam = (TienHang * hd.TiLeGiamGia / 100).GetValueOrDefault();
            lbTienGiam.Text = TienGiam.ToString("N0");
            int TienTra = TienHang - TienGiam;
            lbTienTra.Text = TienTra.ToString("N0");
        }

        private void frmHDChiTiet_FormClosed(object sender, FormClosedEventArgs e)
        {
            //HoaDon hd = new HoaDon();
            //hd.MaHD = maHD;
            //if (maCoupon != 0)
            //    hd.MaCoupon = maCoupon;
            //hd.TongTienHang = TienHang;
            //hd.SoTienDuocGiam = TienGiam;
            //HoaDonBUS.CapNhatHoaDon(hd);
        }

        private void btnCapNhat_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count <= 0)
            {
                MessageBox.Show("Hãy chọn một món hàng cần cật nhật số lượng");
                return;
            }
            int macthd = ((ChiTietHoaDonDTO)dgv.CurrentRow.DataBoundItem).MaCTHD;
            frmSoLuong frm = new frmSoLuong(macthd);
            ChildFrm = frm;
            frm.ShowDialog();
            frmHDChiTiet_Load(null,null);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count <= 0)
            {
                MessageBox.Show("Hãy chọn một món hàng cần xóa khỏi danh sách");
                return;
            }
            int macthd = ((ChiTietHoaDonDTO)dgv.CurrentRow.DataBoundItem).MaCTHD;
            Xử_lý.ChiTietHoaDonBUS.XoaCTHD(macthd);
            MessageBox.Show("Xóa thành công");
            frmHDChiTiet_Load(null, null);
        }

        private void btnThanhToan_Click(object sender, EventArgs e)
        {
            HoaDon hd_new = HoaDonBUS.TimHoaDon(maHD);
            if (maCoupon != 0)
                hd_new.MaCoupon = maCoupon;
            hd_new.TongTienHang = TienHang;
            hd_new.SoTienDuocGiam = TienGiam;
            hd_new.TinhTrangHD = "Đã thanh toán";
            hd_new.HinhThucMuaHang = "Mua tại cửa hàng";
            hd_new.NgayLapHD = DateTime.Now;
           
            HoaDonBUS.CapNhatHoaDon(hd_new);
            MessageBox.Show("Đã thanh toán thành công");
            this.Close();
        }
    }
}
