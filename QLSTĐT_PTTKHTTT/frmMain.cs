﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLSTĐT_PTTKHTTT
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void btnThemSP_Click(object sender, EventArgs e)
        {
            frmSPQuanLy frm = new frmSPQuanLy();
            frm.ShowDialog();
        }

        private void btnThemHD_Click(object sender, EventArgs e)
        {
            frmHDQuanLy frm = new frmHDQuanLy();
            frm.ShowDialog();
        }

        private void btnThemDDH_Click(object sender, EventArgs e)
        {
            frmDHQuanLy frm = new frmDHQuanLy();
            frm.ShowDialog();
        }
    }
}
