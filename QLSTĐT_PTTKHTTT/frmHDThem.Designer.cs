﻿namespace QLSTĐT_PTTKHTTT
{
    partial class frmHDThem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnTimKiem = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.numGiaTienMax = new System.Windows.Forms.NumericUpDown();
            this.numGiaTienMin = new System.Windows.Forms.NumericUpDown();
            this.cbxTenHSX = new System.Windows.Forms.ComboBox();
            this.txtSanPham = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnThanhToan = new System.Windows.Forms.Button();
            this.btnThemSP = new System.Windows.Forms.Button();
            this.btnCapNhatGioHang = new System.Windows.Forms.Button();
            this.dgvDSSanPham = new System.Windows.Forms.DataGridView();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numGiaTienMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numGiaTienMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDSSanPham)).BeginInit();
            this.SuspendLayout();
            // 
            // btnTimKiem
            // 
            this.btnTimKiem.Location = new System.Drawing.Point(575, 57);
            this.btnTimKiem.Name = "btnTimKiem";
            this.btnTimKiem.Size = new System.Drawing.Size(125, 37);
            this.btnTimKiem.TabIndex = 21;
            this.btnTimKiem.Text = "Tìm kiếm";
            this.btnTimKiem.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(528, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "Đến";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(417, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Từ";
            // 
            // numGiaTienMax
            // 
            this.numGiaTienMax.Location = new System.Drawing.Point(562, 11);
            this.numGiaTienMax.Name = "numGiaTienMax";
            this.numGiaTienMax.Size = new System.Drawing.Size(79, 20);
            this.numGiaTienMax.TabIndex = 17;
            // 
            // numGiaTienMin
            // 
            this.numGiaTienMin.Location = new System.Drawing.Point(443, 11);
            this.numGiaTienMin.Name = "numGiaTienMin";
            this.numGiaTienMin.Size = new System.Drawing.Size(79, 20);
            this.numGiaTienMin.TabIndex = 18;
            // 
            // cbxTenHSX
            // 
            this.cbxTenHSX.FormattingEnabled = true;
            this.cbxTenHSX.Location = new System.Drawing.Point(116, 41);
            this.cbxTenHSX.Name = "cbxTenHSX";
            this.cbxTenHSX.Size = new System.Drawing.Size(113, 21);
            this.cbxTenHSX.TabIndex = 16;
            // 
            // txtSanPham
            // 
            this.txtSanPham.Location = new System.Drawing.Point(116, 10);
            this.txtSanPham.Name = "txtSanPham";
            this.txtSanPham.Size = new System.Drawing.Size(113, 20);
            this.txtSanPham.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Tên HSX:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(357, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Giá tiền:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Tên sản phẩm:";
            // 
            // btnThanhToan
            // 
            this.btnThanhToan.Location = new System.Drawing.Point(450, 392);
            this.btnThanhToan.Name = "btnThanhToan";
            this.btnThanhToan.Size = new System.Drawing.Size(86, 35);
            this.btnThanhToan.TabIndex = 9;
            this.btnThanhToan.Text = "Thanh toán";
            this.btnThanhToan.UseVisualStyleBackColor = true;
            this.btnThanhToan.Click += new System.EventHandler(this.btnThanhToan_Click);
            // 
            // btnThemSP
            // 
            this.btnThemSP.Location = new System.Drawing.Point(285, 392);
            this.btnThemSP.Name = "btnThemSP";
            this.btnThemSP.Size = new System.Drawing.Size(86, 35);
            this.btnThemSP.TabIndex = 10;
            this.btnThemSP.Text = "Thêm sản phẩm vào giỏ hàng";
            this.btnThemSP.UseVisualStyleBackColor = true;
            this.btnThemSP.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnCapNhatGioHang
            // 
            this.btnCapNhatGioHang.Location = new System.Drawing.Point(116, 392);
            this.btnCapNhatGioHang.Name = "btnCapNhatGioHang";
            this.btnCapNhatGioHang.Size = new System.Drawing.Size(86, 35);
            this.btnCapNhatGioHang.TabIndex = 11;
            this.btnCapNhatGioHang.Text = "Cập nhật giỏ hàng";
            this.btnCapNhatGioHang.UseVisualStyleBackColor = true;
            this.btnCapNhatGioHang.Click += new System.EventHandler(this.btnCapNhatGioHang_Click);
            // 
            // dgvDSSanPham
            // 
            this.dgvDSSanPham.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDSSanPham.Location = new System.Drawing.Point(12, 100);
            this.dgvDSSanPham.Name = "dgvDSSanPham";
            this.dgvDSSanPham.Size = new System.Drawing.Size(688, 286);
            this.dgvDSSanPham.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 84);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(132, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Danh sách các sản phẩm:";
            // 
            // frmHDThem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(715, 436);
            this.Controls.Add(this.btnTimKiem);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.numGiaTienMax);
            this.Controls.Add(this.numGiaTienMin);
            this.Controls.Add(this.cbxTenHSX);
            this.Controls.Add(this.txtSanPham);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnThanhToan);
            this.Controls.Add(this.btnThemSP);
            this.Controls.Add(this.btnCapNhatGioHang);
            this.Controls.Add(this.dgvDSSanPham);
            this.Name = "frmHDThem";
            this.Text = "Thêm sản phẩm vào hóa đơn";
            this.Load += new System.EventHandler(this.frmHDThem_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numGiaTienMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numGiaTienMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDSSanPham)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnTimKiem;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numGiaTienMax;
        private System.Windows.Forms.NumericUpDown numGiaTienMin;
        private System.Windows.Forms.ComboBox cbxTenHSX;
        private System.Windows.Forms.TextBox txtSanPham;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnThanhToan;
        private System.Windows.Forms.Button btnThemSP;
        private System.Windows.Forms.Button btnCapNhatGioHang;
        private System.Windows.Forms.DataGridView dgvDSSanPham;
        private System.Windows.Forms.Label label6;
    }
}