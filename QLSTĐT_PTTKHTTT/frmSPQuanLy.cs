﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLSTĐT_PTTKHTTT
{
    public partial class frmSPQuanLy : Form
    {
        public frmSPQuanLy()
        {
            InitializeComponent();
        }

        private void frmSPQuanLy_Load(object sender, EventArgs e)
        {
            dgv.DataSource = Xử_lý.SanPhamBUS.LayDSSanPham();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmSPThem frm = new frmSPThem();
            frm.ShowDialog();

        }
    }
}
