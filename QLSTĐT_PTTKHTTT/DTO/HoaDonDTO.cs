﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLSTĐT_PTTKHTTT.DTO
{
    public class HoaDonDTO
    {
        public int MaHD { get; set; }
        public int MaKH { get; set; }
        public string TenCoupon { get; set; }
        public DateTime? NgayLapHD { get; set; }
        public int? TongTienHang { get; set; }
        public string HinhThucMuaHang { get; set; }
        public int? SoTienDuocGiam { get; set; }
        public int? TiLeGiamGia { get; set; }
        public string LoaiHoaDon { get; set; }
        public string TinhTrangHD { get; set; }
    }
}
