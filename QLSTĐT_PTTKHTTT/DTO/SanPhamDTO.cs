﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLSTĐT_PTTKHTTT.DTO
{
   public class SanPhamDTO
    {
        public int MaSP { get; set; }
        public string TenSP { get; set; }
        public string KichThuoc { get; set; }
        public int? TrongLuong { get; set; }
        public string HDH { get; set; }
        public string MoTaThem { get; set; }
        public int? DonGia { get; set; }
        public string XuatXu { get; set; }
        public int? ThoiGianBH { get; set; }
        public string TenHSX { get; set; }
        public string TinhTrang { get; set; }
        public string PhanLoai { get; set; }

    }
}
