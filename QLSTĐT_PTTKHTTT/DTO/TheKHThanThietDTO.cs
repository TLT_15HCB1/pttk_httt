﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLSTĐT_PTTKHTTT.DTO
{
   public class TheKHThanThietDTO
    {
        public int MaTheKHBT { get; set; }
        public string TenKH { get; set; }
        public string TenNguoiDD { get; set; }
        public string TenCty { get; set; }
        public string DiaChi { get; set; }
        public string PhanLoai { get; set; }
    }
}
