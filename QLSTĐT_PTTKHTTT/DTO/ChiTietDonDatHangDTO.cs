﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLSTĐT_PTTKHTTT.DTO
{
    public class ChiTietDonDatHangDTO
    {
        public int MaCTDDH { get; set;}
        public int? MaDDH { get; set; }
        public int? MaSP { get; set; }
        public int? SoLuong { get; set; }
        public int? DonGia { get; set; }
    }
}
