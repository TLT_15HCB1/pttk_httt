﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLSTĐT_PTTKHTTT.DTO
{
    public class DonDatHangDTO
    {
        public int MaDDH { get; set; }
        public int? MaNCC { get; set; }
        public DateTime? NgayLapDon { get; set; }
        public DateTime? NgayHenGiaoHang { get; set; }
        public int? TongTienHang { get; set; }
        public string TinhTrangDonHang { get; set; }
    }
}
