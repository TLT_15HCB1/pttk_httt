﻿
namespace QLSTĐT_PTTKHTTT.Xử_lý
{
    public class ChiTietHoaDonDTO
    {
        public int MaCTHD { get; set; }
        public int MaHD { get; set; }

        public string TenSP { get; set; }
        public string HSX { get; set; }
        public int? SoLuong { get; set; }
        public int? DonGia { get; set; }

    }
}