﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLSTĐT_PTTKHTTT.DTO
{
   public class TheKhachHangDTO
    {
        public int MaTheKH { get; set; }
        public int DiemTichLuy { get; set; }
        public string LoaiKH { get; set; }
    }
}
