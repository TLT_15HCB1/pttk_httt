﻿namespace QLSTĐT_PTTKHTTT
{
    partial class frmSPThem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtTenSP = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbHSX = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.numDonGia = new System.Windows.Forms.NumericUpDown();
            this.txtXuatXu = new System.Windows.Forms.TextBox();
            this.btnThemSP = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.cbLoaiSP = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtKichThuoc = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtHDH = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtMoTaThem = new System.Windows.Forms.TextBox();
            this.numThoiGianBH = new System.Windows.Forms.NumericUpDown();
            this.numTrongLuong = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.numDonGia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numThoiGianBH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTrongLuong)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tên sản phẩm:";
            // 
            // txtTenSP
            // 
            this.txtTenSP.Location = new System.Drawing.Point(134, 34);
            this.txtTenSP.Name = "txtTenSP";
            this.txtTenSP.Size = new System.Drawing.Size(179, 20);
            this.txtTenSP.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Hãng sản xuất";
            // 
            // cbHSX
            // 
            this.cbHSX.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbHSX.FormattingEnabled = true;
            this.cbHSX.Location = new System.Drawing.Point(134, 66);
            this.cbHSX.Name = "cbHSX";
            this.cbHSX.Size = new System.Drawing.Size(179, 21);
            this.cbHSX.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Đơn giá:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(34, 127);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Xuất xứ:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(33, 158);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Thời gian BH:";
            // 
            // numDonGia
            // 
            this.numDonGia.Location = new System.Drawing.Point(134, 97);
            this.numDonGia.Maximum = new decimal(new int[] {
            1316134911,
            2328,
            0,
            0});
            this.numDonGia.Name = "numDonGia";
            this.numDonGia.Size = new System.Drawing.Size(179, 20);
            this.numDonGia.TabIndex = 3;
            // 
            // txtXuatXu
            // 
            this.txtXuatXu.Location = new System.Drawing.Point(134, 124);
            this.txtXuatXu.Name = "txtXuatXu";
            this.txtXuatXu.Size = new System.Drawing.Size(179, 20);
            this.txtXuatXu.TabIndex = 1;
            // 
            // btnThemSP
            // 
            this.btnThemSP.Location = new System.Drawing.Point(117, 385);
            this.btnThemSP.Name = "btnThemSP";
            this.btnThemSP.Size = new System.Drawing.Size(120, 37);
            this.btnThemSP.TabIndex = 4;
            this.btnThemSP.Text = "Thêm sản phẩm";
            this.btnThemSP.UseVisualStyleBackColor = true;
            this.btnThemSP.Click += new System.EventHandler(this.btnThemSP_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(33, 192);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Loại sản phẩm";
            // 
            // cbLoaiSP
            // 
            this.cbLoaiSP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbLoaiSP.FormattingEnabled = true;
            this.cbLoaiSP.Items.AddRange(new object[] {
            "Điện thoại di động",
            "Khác"});
            this.cbLoaiSP.Location = new System.Drawing.Point(134, 192);
            this.cbLoaiSP.Name = "cbLoaiSP";
            this.cbLoaiSP.Size = new System.Drawing.Size(179, 21);
            this.cbLoaiSP.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(33, 225);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Kích thước";
            // 
            // txtKichThuoc
            // 
            this.txtKichThuoc.Location = new System.Drawing.Point(134, 222);
            this.txtKichThuoc.Name = "txtKichThuoc";
            this.txtKichThuoc.Size = new System.Drawing.Size(179, 20);
            this.txtKichThuoc.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(33, 254);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Trọng lượng";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(33, 285);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Hệ điều hành";
            // 
            // txtHDH
            // 
            this.txtHDH.Location = new System.Drawing.Point(134, 282);
            this.txtHDH.Name = "txtHDH";
            this.txtHDH.Size = new System.Drawing.Size(179, 20);
            this.txtHDH.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(33, 314);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Mô tả thêm";
            // 
            // txtMoTaThem
            // 
            this.txtMoTaThem.Location = new System.Drawing.Point(134, 311);
            this.txtMoTaThem.Name = "txtMoTaThem";
            this.txtMoTaThem.Size = new System.Drawing.Size(179, 20);
            this.txtMoTaThem.TabIndex = 1;
            // 
            // numThoiGianBH
            // 
            this.numThoiGianBH.Location = new System.Drawing.Point(134, 156);
            this.numThoiGianBH.Name = "numThoiGianBH";
            this.numThoiGianBH.Size = new System.Drawing.Size(179, 20);
            this.numThoiGianBH.TabIndex = 3;
            // 
            // numTrongLuong
            // 
            this.numTrongLuong.Location = new System.Drawing.Point(134, 252);
            this.numTrongLuong.Maximum = new decimal(new int[] {
            1316134911,
            2328,
            0,
            0});
            this.numTrongLuong.Name = "numTrongLuong";
            this.numTrongLuong.Size = new System.Drawing.Size(179, 20);
            this.numTrongLuong.TabIndex = 3;
            // 
            // frmSPThem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(373, 434);
            this.Controls.Add(this.btnThemSP);
            this.Controls.Add(this.numThoiGianBH);
            this.Controls.Add(this.numTrongLuong);
            this.Controls.Add(this.numDonGia);
            this.Controls.Add(this.cbLoaiSP);
            this.Controls.Add(this.cbHSX);
            this.Controls.Add(this.txtMoTaThem);
            this.Controls.Add(this.txtHDH);
            this.Controls.Add(this.txtKichThuoc);
            this.Controls.Add(this.txtXuatXu);
            this.Controls.Add(this.txtTenSP);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "frmSPThem";
            this.Text = "Thêm sản phẩm";
            this.Load += new System.EventHandler(this.frmSPThem_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numDonGia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numThoiGianBH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTrongLuong)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTenSP;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbHSX;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numDonGia;
        private System.Windows.Forms.TextBox txtXuatXu;
        private System.Windows.Forms.Button btnThemSP;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbLoaiSP;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtKichThuoc;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtHDH;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtMoTaThem;
        private System.Windows.Forms.NumericUpDown numThoiGianBH;
        private System.Windows.Forms.NumericUpDown numTrongLuong;
    }
}

