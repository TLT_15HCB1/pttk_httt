﻿namespace QLSTĐT_PTTKHTTT
{
    partial class frmHDChiTiet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgv = new System.Windows.Forms.DataGridView();
            this.btnThanhToan = new System.Windows.Forms.Button();
            this.btnXoa = new System.Windows.Forms.Button();
            this.btnCapNhat = new System.Windows.Forms.Button();
            this.numMaCoupon = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCoupon = new System.Windows.Forms.Button();
            this.lblTienHang = new System.Windows.Forms.Label();
            this.lblTienGiam = new System.Windows.Forms.Label();
            this.lblTienTra = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbTienHang = new System.Windows.Forms.Label();
            this.lbTienGiam = new System.Windows.Forms.Label();
            this.lbTienTra = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMaCoupon)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(12, 72);
            this.dgv.Name = "dgv";
            this.dgv.Size = new System.Drawing.Size(660, 269);
            this.dgv.TabIndex = 9;
            // 
            // btnThanhToan
            // 
            this.btnThanhToan.Location = new System.Drawing.Point(34, 436);
            this.btnThanhToan.Name = "btnThanhToan";
            this.btnThanhToan.Size = new System.Drawing.Size(181, 33);
            this.btnThanhToan.TabIndex = 10;
            this.btnThanhToan.Text = "Thanh toán";
            this.btnThanhToan.UseVisualStyleBackColor = true;
            this.btnThanhToan.Click += new System.EventHandler(this.btnThanhToan_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Location = new System.Drawing.Point(34, 397);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(181, 33);
            this.btnXoa.TabIndex = 10;
            this.btnXoa.Text = "Xóa khỏi danh sách";
            this.btnXoa.UseVisualStyleBackColor = true;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnCapNhat
            // 
            this.btnCapNhat.Location = new System.Drawing.Point(34, 358);
            this.btnCapNhat.Name = "btnCapNhat";
            this.btnCapNhat.Size = new System.Drawing.Size(181, 33);
            this.btnCapNhat.TabIndex = 10;
            this.btnCapNhat.Text = "Cập nhật số lượng";
            this.btnCapNhat.UseVisualStyleBackColor = true;
            this.btnCapNhat.Click += new System.EventHandler(this.btnCapNhat_Click);
            // 
            // numMaCoupon
            // 
            this.numMaCoupon.Location = new System.Drawing.Point(99, 12);
            this.numMaCoupon.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.numMaCoupon.Name = "numMaCoupon";
            this.numMaCoupon.Size = new System.Drawing.Size(87, 20);
            this.numMaCoupon.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Mã Coupon:";
            // 
            // btnCoupon
            // 
            this.btnCoupon.Location = new System.Drawing.Point(192, 9);
            this.btnCoupon.Name = "btnCoupon";
            this.btnCoupon.Size = new System.Drawing.Size(75, 23);
            this.btnCoupon.TabIndex = 13;
            this.btnCoupon.Text = "Áp dụng";
            this.btnCoupon.UseVisualStyleBackColor = true;
            this.btnCoupon.Click += new System.EventHandler(this.btnCoupon_Click);
            // 
            // lblTienHang
            // 
            this.lblTienHang.AutoSize = true;
            this.lblTienHang.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTienHang.Location = new System.Drawing.Point(374, 358);
            this.lblTienHang.Name = "lblTienHang";
            this.lblTienHang.Size = new System.Drawing.Size(115, 16);
            this.lblTienHang.TabIndex = 15;
            this.lblTienHang.Text = "Tổng tiền hàng:";
            // 
            // lblTienGiam
            // 
            this.lblTienGiam.AutoSize = true;
            this.lblTienGiam.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTienGiam.Location = new System.Drawing.Point(374, 397);
            this.lblTienGiam.Name = "lblTienGiam";
            this.lblTienGiam.Size = new System.Drawing.Size(136, 16);
            this.lblTienGiam.TabIndex = 15;
            this.lblTienGiam.Text = "Số tiền được giảm:";
            // 
            // lblTienTra
            // 
            this.lblTienTra.AutoSize = true;
            this.lblTienTra.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTienTra.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lblTienTra.Location = new System.Drawing.Point(374, 436);
            this.lblTienTra.Name = "lblTienTra";
            this.lblTienTra.Size = new System.Drawing.Size(165, 25);
            this.lblTienTra.TabIndex = 15;
            this.lblTienTra.Text = "Số tiền phải trả:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(200, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Danh sách các mặt hàng trong hóa đơn:";
            // 
            // lbTienHang
            // 
            this.lbTienHang.AutoSize = true;
            this.lbTienHang.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTienHang.Location = new System.Drawing.Point(495, 358);
            this.lbTienHang.Name = "lbTienHang";
            this.lbTienHang.Size = new System.Drawing.Size(16, 16);
            this.lbTienHang.TabIndex = 15;
            this.lbTienHang.Text = "0";
            // 
            // lbTienGiam
            // 
            this.lbTienGiam.AutoSize = true;
            this.lbTienGiam.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTienGiam.Location = new System.Drawing.Point(516, 397);
            this.lbTienGiam.Name = "lbTienGiam";
            this.lbTienGiam.Size = new System.Drawing.Size(16, 16);
            this.lbTienGiam.TabIndex = 15;
            this.lbTienGiam.Text = "0";
            // 
            // lbTienTra
            // 
            this.lbTienTra.AutoSize = true;
            this.lbTienTra.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTienTra.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lbTienTra.Location = new System.Drawing.Point(536, 436);
            this.lbTienTra.Name = "lbTienTra";
            this.lbTienTra.Size = new System.Drawing.Size(24, 25);
            this.lbTienTra.TabIndex = 15;
            this.lbTienTra.Text = "0";
            // 
            // frmHDChiTiet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 476);
            this.Controls.Add(this.lbTienTra);
            this.Controls.Add(this.lblTienTra);
            this.Controls.Add(this.lbTienGiam);
            this.Controls.Add(this.lblTienGiam);
            this.Controls.Add(this.lbTienHang);
            this.Controls.Add(this.lblTienHang);
            this.Controls.Add(this.btnCoupon);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numMaCoupon);
            this.Controls.Add(this.btnCapNhat);
            this.Controls.Add(this.btnXoa);
            this.Controls.Add(this.btnThanhToan);
            this.Controls.Add(this.dgv);
            this.Name = "frmHDChiTiet";
            this.Text = "Chi tiết hóa đơn";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmHDChiTiet_FormClosed);
            this.Load += new System.EventHandler(this.frmHDChiTiet_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMaCoupon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Button btnThanhToan;
        private System.Windows.Forms.Button btnXoa;
        private System.Windows.Forms.Button btnCapNhat;
        private System.Windows.Forms.NumericUpDown numMaCoupon;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCoupon;
        private System.Windows.Forms.Label lblTienHang;
        private System.Windows.Forms.Label lblTienGiam;
        private System.Windows.Forms.Label lblTienTra;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbTienHang;
        private System.Windows.Forms.Label lbTienGiam;
        private System.Windows.Forms.Label lbTienTra;
    }
}