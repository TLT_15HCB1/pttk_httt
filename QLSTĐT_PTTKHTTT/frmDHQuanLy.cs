﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLSTĐT_PTTKHTTT
{
    public partial class frmDHQuanLy : Form
    {
        public frmDHQuanLy()
        {
            InitializeComponent();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            frmDHThem f = new frmDHThem();
            f.ShowDialog();
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            
        }

        private void frmDHQuanLy_Load(object sender, EventArgs e)
        {
            dgvDDH.DataSource = QLSTĐT_PTTKHTTT.Xử_lý.DonDatHangBUS.LayDSDonDH();
        }
    }
}
