﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QLSTĐT_PTTKHTTT.DTO;

namespace QLSTĐT_PTTKHTTT
{
    public partial class frmDHThem : Form
    {
        int maDDH = -1;
        bool check = false;
        public frmDHThem()
        {
            InitializeComponent();
        }        

        private void frmDHThem_Load(object sender, EventArgs e)
        {
            dgvCTDDH.DataSource = Xử_lý.SanPhamBUS.LayDSSanPham();

            cboHSX.DataSource = Xử_lý.HangSanXuatBUS.LoadcbxHSX();
            cboHSX.ValueMember = "MaHSX";
            cboHSX.DisplayMember = "TenHSX";

            cboNCC.DataSource = Xử_lý.NhaCungCapBUS.LoadcbxNCC();
            cboNCC.ValueMember = "MaNCC";
            cboNCC.DisplayMember = "TenNCC";
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            if (dgvCTDDH.SelectedRows.Count <= 0)
            {
                MessageBox.Show("Hãy chọn 1 sản phẩm cần thêm vào hóa đơn");
                return;
            }
            int maSP = ((SanPhamDTO)dgvCTDDH.CurrentRow.DataBoundItem).MaSP;
            if (maDDH == -1)
            {
                ThemDonDatHang();
                ThemCTDDH(maSP);                    
            }
            else
            {
                ThemCTDDH(maSP);
            }
            check = true;
        }

        private void ThemDonDatHang()
        {
            DonDatHang ddh = new DonDatHang();
            ddh.MaNCC = Convert.ToInt32(cboNCC.SelectedValue.ToString());
            ddh.NgayLapDon = DateTime.Now;
            ddh.NgayHenGiaoHang = DateTime.Now;
            ddh.TongTienHang = 0;
            ddh.TinhTrangDonHang = "Chưa giao";
            maDDH = Xử_lý.DonDatHangBUS.ThemDonDH(ddh);
        }

        private int ThemCTDDH(int maSP)
        {
            SanPham sp = Xử_lý.SanPhamBUS.TimSanPham(maSP);
            ChiTietDonDatHang ctddh = new ChiTietDonDatHang();
            ctddh.MaDDH = maDDH;
            ctddh.MaSP = maSP;
            ctddh.SoLuong = 1;
            ctddh.DonGia = sp.DonGia;
            Xử_lý.ChiTietDonDatHangBUS.ThemCTDDH(ctddh);
            return ctddh.MaCTDDH;
        }

        private void btnThanhToan_Click(object sender, EventArgs e)
        {
            if (check == false)
                MessageBox.Show("Chưa thêm hàng vào giỏ", "Thông báo");
            else
            {
                frmDHChiTietcs frm = new frmDHChiTietcs(maDDH);
                frm.ShowDialog();
            }
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            //int tenSP = txtTenSP.Text;
            //dgvCTDDH.DataSource = Xử_lý.SanPhamBUS.TimSanPham(tenSP);
        }

        private void btnCapNhatGioHang_Click(object sender, EventArgs e)
        {
            if (check == false)
                MessageBox.Show("Chưa thêm hàng vào giỏ", "Thông báo");
            else
            {
                frmDHChiTietcs frm = new frmDHChiTietcs(maDDH);
                frm.ShowDialog();
            }
        }
    }
}
