﻿using QLSTĐT_PTTKHTTT.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLSTĐT_PTTKHTTT
{
    public partial class frmHDThem : Form
    {
        private TheKHBinhTHuong khbt;
        private TheKHThanThiet khtt;
        int maHD = -1;

        public frmHDThem()
        {
            InitializeComponent();
        }

        public frmHDThem(TheKHBinhTHuong khbt)
        {
            this.khbt = khbt;
            InitializeComponent();
        }

        public frmHDThem(TheKHThanThiet khtt)
        {
            this.khtt = khtt;
            InitializeComponent();
        }

        private void frmHDThem_Load(object sender, EventArgs e)
        {
            dgvDSSanPham.DataSource = Xử_lý.SanPhamBUS.LayDSSanPham();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dgvDSSanPham.SelectedRows.Count <= 0)
            {
                MessageBox.Show("Hãy chọn 1 sản phẩm cần thêm vào hóa đơn");
                return;
            }

            int maSP =((SanPhamDTO) dgvDSSanPham.CurrentRow.DataBoundItem).MaSP;
            if (maHD == -1)
            {
                if (khbt != null)
                {
                    ThemHoaDon(khbt);
                    ThemCTHD(maSP);
                    
                }
                else if (khtt != null)
                {
                    ThemHoaDon(khtt);
                    ThemCTHD(maSP);
                }
            }
            else
            {
                ThemCTHD(maSP);
            }
            MessageBox.Show("Đã thêm sản phẩm vào hóa đơn");
        }

        private void ThemHoaDon(TheKHBinhTHuong bt)
        {
            HoaDon hd = new HoaDon();
            hd.MaTheKH = bt.MaTheKHBT;
            hd.TinhTrangHD = "Chưa thanh toán";
            hd.TiLeGiamGia = 0;
            hd.LoaiHoaDon = "Hóa đơn bán lẻ";
            hd.TongTienHang = 0;
            maHD = Xử_lý.HoaDonBUS.ThemHoaDon(hd);
        }

        private void ThemHoaDon(TheKHThanThiet tt)
        {
            HoaDon hd = new HoaDon();
            hd.MaTheKH = tt.MaTheKHTT;
            hd.TinhTrangHD = "Chưa thanh toán";
            hd.LoaiHoaDon = "Hóa đơn bán sĩ";
            hd.TiLeGiamGia = tt.CapDo;
            hd.TongTienHang = 0;
            maHD = Xử_lý.HoaDonBUS.ThemHoaDon(hd);
        }

        private int ThemCTHD (int maSP)
        {
            SanPham sp = Xử_lý.SanPhamBUS.TimSanPham(maSP);
            ChiTietHoaDon cthd = new ChiTietHoaDon();
            cthd.MaHD = maHD;
            cthd.MaSP = maSP;
            cthd.SoLuong = 1;
            cthd.DonGia = sp.DonGia;
            Xử_lý.ChiTietHoaDonBUS.ThemCTHD(cthd);
            return cthd.MaCTHD;
        }

        private void btnCapNhatGioHang_Click(object sender, EventArgs e)
        {
            frmHDChiTiet frm = new frmHDChiTiet(maHD);
            frm.ShowDialog();
        }

        private void btnThanhToan_Click(object sender, EventArgs e)
        {
            frmHDChiTiet frm = new frmHDChiTiet(maHD);
            frm.ShowDialog();
        }
    }
}
