﻿using QLSTĐT_PTTKHTTT.Xử_lý;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLSTĐT_PTTKHTTT
{
    public partial class frmSPThem : Form
    {
        public frmSPThem()
        {
            InitializeComponent();
        }

        private void frmSPThem_Load(object sender, EventArgs e)
        {
            cbLoaiSP.SelectedIndex = 0;
            List<HSXDTO> a = Xử_lý.HangSanXuatBUS.LoadcbxHSX();
            cbHSX.DataSource = a;
            cbHSX.DisplayMember = "TenHSX";
            cbHSX.ValueMember = "MaHSX";
        }

        private void btnThemSP_Click(object sender, EventArgs e)
        {
            int id = -1;
            SanPham sp = new SanPham();
            sp.TenSP = txtTenSP.Text;
            sp.DonGia = (int)numDonGia.Value;
            sp.MaHSX = (int)cbHSX.SelectedValue;
            sp.HDH = txtHDH.Text;
            sp.KichThuoc = txtKichThuoc.Text;
            sp.MoTaThem = txtMoTaThem.Text;
            sp.PhanLoai = cbLoaiSP.Text;
            sp.ThoiGianBH = (int)numThoiGianBH.Value;
            sp.TinhTrang = "Hết hàng";
            sp.XuatXu = txtXuatXu.Text;
            sp.TrongLuong = (int)numTrongLuong.Value;
             id = Xử_lý.SanPhamBUS.ThemSanPham(sp);
            if (id != -1)
                MessageBox.Show("Thêm sản phẩm thành công");
            else
                MessageBox.Show("Có lỗi xảy ra");
        }


    }
}
