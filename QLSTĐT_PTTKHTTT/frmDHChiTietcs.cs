﻿using QLSTĐT_PTTKHTTT.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLSTĐT_PTTKHTTT
{
    public partial class frmDHChiTietcs : Form
    {
        private int maDDH;
        private frmSoLuong ChildFrm;
        public frmDHChiTietcs()
        {
            InitializeComponent();
        }

        public frmDHChiTietcs(int maDDH)
        {
            this.maDDH = maDDH;
            InitializeComponent();
        }

        private void frmDHChiTietcs_Load(object sender, EventArgs e)
        {
            dgvCTDDH.DataSource = Xử_lý.ChiTietDonDatHangBUS.LayDSChiTietDDH(maDDH);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            int icountSelectedRow = dgvCTDDH.SelectedRows.Count;
            int maCTDDH;
            if (icountSelectedRow == 0)
                MessageBox.Show("Bạn hãy chọn dòng cần xoá!");
            else
                foreach (DataGridViewRow row in dgvCTDDH.SelectedRows)
                    if (!row.IsNewRow)
                    {
                        maCTDDH = Convert.ToInt32(dgvCTDDH.CurrentRow.Cells[0].Value.ToString());
                        Xử_lý.ChiTietDonDatHangBUS.XoaCTDDH(maCTDDH);
                        dgvCTDDH.DataSource = Xử_lý.ChiTietDonDatHangBUS.LayDSChiTietDDH(maDDH);
                    }
        }

        private void btnXacNhan_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Đã đặt hàng!!!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
        }

        private void btnCapNhat_Click(object sender, EventArgs e)
        {

        }
    }
}
