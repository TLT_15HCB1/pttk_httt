﻿using QLSTĐT_PTTKHTTT.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLSTĐT_PTTKHTTT
{
    public partial class frmHDQuanLy : Form
    {
        public frmHDQuanLy()
        {
            InitializeComponent();
        }

        private void frmHDQuanLy_Load(object sender, EventArgs e)
        {
            dgvDSHoaDon.DataSource = Xử_lý.HoaDonBUS.LayDSHoaDon();
            
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            frmMaKH frm = new frmMaKH();
            frm.ShowDialog();
        }

        private void btnChiTiet_Click(object sender, EventArgs e)
        {
            if (dgvDSHoaDon.SelectedRows.Count <= 0)
            {
                MessageBox.Show("Hãy chọn hóa đơn cần xem chi tiết");
                return;
            }
            else
            {
                int maHD = ((HoaDonDTO)dgvDSHoaDon.CurrentRow.DataBoundItem).MaHD;
                frmHDChiTiet frm = new frmHDChiTiet(maHD);
                frm.ShowDialog();
            }
        }
    }
}
