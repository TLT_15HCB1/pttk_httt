﻿namespace QLSTĐT_PTTKHTTT
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnThemSP = new System.Windows.Forms.Button();
            this.btnThemHD = new System.Windows.Forms.Button();
            this.btnThemDDH = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnThemSP
            // 
            this.btnThemSP.Location = new System.Drawing.Point(33, 19);
            this.btnThemSP.Name = "btnThemSP";
            this.btnThemSP.Size = new System.Drawing.Size(119, 54);
            this.btnThemSP.TabIndex = 0;
            this.btnThemSP.Text = "Quản lý sản phẩm";
            this.btnThemSP.UseVisualStyleBackColor = true;
            this.btnThemSP.Click += new System.EventHandler(this.btnThemSP_Click);
            // 
            // btnThemHD
            // 
            this.btnThemHD.Location = new System.Drawing.Point(33, 79);
            this.btnThemHD.Name = "btnThemHD";
            this.btnThemHD.Size = new System.Drawing.Size(119, 56);
            this.btnThemHD.TabIndex = 1;
            this.btnThemHD.Text = "Thêm hóa đơn";
            this.btnThemHD.UseVisualStyleBackColor = true;
            this.btnThemHD.Click += new System.EventHandler(this.btnThemHD_Click);
            // 
            // btnThemDDH
            // 
            this.btnThemDDH.Location = new System.Drawing.Point(33, 141);
            this.btnThemDDH.Name = "btnThemDDH";
            this.btnThemDDH.Size = new System.Drawing.Size(119, 60);
            this.btnThemDDH.TabIndex = 1;
            this.btnThemDDH.Text = "Thêm đơn đặt hàng";
            this.btnThemDDH.UseVisualStyleBackColor = true;
            this.btnThemDDH.Click += new System.EventHandler(this.btnThemDDH_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(179, 215);
            this.Controls.Add(this.btnThemDDH);
            this.Controls.Add(this.btnThemHD);
            this.Controls.Add(this.btnThemSP);
            this.Name = "frmMain";
            this.Text = "frmMain";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnThemSP;
        private System.Windows.Forms.Button btnThemHD;
        private System.Windows.Forms.Button btnThemDDH;
    }
}