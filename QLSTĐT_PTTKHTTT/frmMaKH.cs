﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLSTĐT_PTTKHTTT
{
    public partial class frmMaKH : Form
    {
        public frmMaKH()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int MaTheKH = (int)numMaKH.Value;

            using (QLSieuThiDTEntities ctx = new QLSieuThiDTEntities())
            {
                TheKhachHang kh = ctx.TheKhachHangs.FirstOrDefault(c => c.MaTheKH == MaTheKH);
                if (kh == null)
                {
                    MessageBox.Show("Mã thẻ khách hàng không tồn tại");
                    return;
                }
                TheKHBinhTHuong khbt;
                TheKHThanThiet khtt;
                
                if (kh.LoaiKH == "Bình thường")
                {
                    khbt = ctx.TheKHBinhTHuongs.FirstOrDefault(c => c.MaTheKHBT == MaTheKH);
                    frmHDThem frm = new frmHDThem(khbt);
                    frm.ShowDialog();
                }
                else if (kh.LoaiKH == "Thân thiết")
                {
                    khtt = ctx.TheKHThanThiets.FirstOrDefault(c => c.MaTheKHTT == MaTheKH);
                    frmHDThem frm = new frmHDThem(khtt);
                    frm.ShowDialog();
                }
            }
        }
    }
}
