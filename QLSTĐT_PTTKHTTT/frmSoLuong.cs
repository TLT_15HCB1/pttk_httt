﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLSTĐT_PTTKHTTT
{
    public partial class frmSoLuong : Form
    {
        public int macthd;
        public frmSoLuong(int macthd)
        {
            InitializeComponent();
            this.macthd = macthd;
        }

        private void btnSL_Click(object sender, EventArgs e)
        {
            Xử_lý.ChiTietHoaDonBUS.CapNhatSL(macthd, (int)numSL.Value);
            MessageBox.Show("Cập nhật số lượng hàng thành công");
            this.Close();
        }
    }
}
