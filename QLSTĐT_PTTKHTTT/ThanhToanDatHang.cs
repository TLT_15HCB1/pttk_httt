//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QLSTĐT_PTTKHTTT
{
    using System;
    using System.Collections.Generic;
    
    public partial class ThanhToanDatHang
    {
        public int MaTTDH { get; set; }
        public int MaDDH { get; set; }
        public Nullable<System.DateTime> NgayTra { get; set; }
        public string SoTaiKhoan { get; set; }
        public string LoaiThanhToan { get; set; }
    
        public virtual DonDatHang DonDatHang { get; set; }
    }
}
