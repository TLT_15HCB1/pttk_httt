﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QLSTĐT_PTTKHTTT.DTO;

namespace QLSTĐT_PTTKHTTT.Xử_lý
{
    public static class DonDatHangBUS
    {
        public static List<DonDatHangDTO> LayDSDonDH()
        {
            using (var ctx = new QLSieuThiDTEntities())
            {
                var query = from c in ctx.DonDatHangs
                            select new DonDatHangDTO
                            {
                                MaDDH = c.MaDDH,
                                MaNCC = c.MaNCC,
                                NgayLapDon = c.NgayLapDon,
                                NgayHenGiaoHang = c.NgayHenGiaoHang,
                                TongTienHang = c.TongTienHang,
                                TinhTrangDonHang = c.TinhTrangDonHang
                            };
                return query.ToList();
            }
        }

        public static int ThemDonDH(DonDatHang ddh)
        {
            using (QLSieuThiDTEntities ctx = new QLSieuThiDTEntities())
            {
                ctx.DonDatHangs.Add(ddh);
                ctx.SaveChanges();
                return ddh.MaDDH;
            }
        }


    }
}
