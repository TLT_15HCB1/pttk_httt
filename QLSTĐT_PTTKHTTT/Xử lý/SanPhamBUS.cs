﻿using QLSTĐT_PTTKHTTT.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLSTĐT_PTTKHTTT.Xử_lý
{

    public static class SanPhamBUS
    {
        public static int ThemSanPham(SanPham sp)
        {
            using (QLSieuThiDTEntities ctx = new QLSieuThiDTEntities())
            {
                var newSp = new SanPham
                {
                    TenSP = sp.TenSP,
                    DonGia = sp.DonGia,
                    XuatXu = sp.XuatXu,
                    ThoiGianBH = sp.ThoiGianBH,
                    MaHSX = sp.MaHSX,
                    TinhTrang = sp.TinhTrang,
                    PhanLoai = sp.PhanLoai,
                    KichThuoc = sp.KichThuoc,
                    TrongLuong = sp.TrongLuong,
                    HDH = sp.HDH,
                    MoTaThem = sp.MoTaThem,
                };
                ctx.SanPhams.Add(newSp);
                ctx.SaveChanges();
                return (newSp.MaSP);
            }
        }

        public static List<SanPhamDTO> LayDSSanPham()
        {
            using (QLSieuThiDTEntities ctx = new QLSieuThiDTEntities())
            {
                var query = from c in ctx.SanPhams
                            select new SanPhamDTO 
                            {
                                MaSP = c.MaSP,
                                TenSP = c.TenSP,
                                KichThuoc = c.KichThuoc,
                                TrongLuong = c.TrongLuong,
                                HDH = c.HDH,
                                MoTaThem = c.MoTaThem,
                                DonGia = c.DonGia,
                                XuatXu = c.XuatXu,
                                ThoiGianBH = c.ThoiGianBH,
                                TenHSX = c.HangSanXuat.TenHSX,
                                TinhTrang = c.TinhTrang,
                                PhanLoai = c.PhanLoai,
                            };
                return query.ToList();
            }
        }

        public static SanPham TimSanPham(int maSP)
        {
            using (QLSieuThiDTEntities ctx = new QLSieuThiDTEntities())
            {
                return ctx.SanPhams.FirstOrDefault(c => c.MaSP == maSP);
            }
        }
    }
}
