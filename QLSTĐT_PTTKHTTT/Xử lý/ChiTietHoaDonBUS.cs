﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLSTĐT_PTTKHTTT.Xử_lý
{
    public static class ChiTietHoaDonBUS
    {
        public static List<ChiTietHoaDonDTO> LayDSChiTietHoaDon(int maHD)
        {
            using (QLSieuThiDTEntities ctx = new QLSieuThiDTEntities())
            {
                var query = from c in ctx.ChiTietHoaDons
                            where c.MaHD == maHD
                            select new ChiTietHoaDonDTO
                            {
                                MaCTHD = c.MaCTHD,
                                MaHD = c.MaHD,
                                TenSP = c.SanPham.TenSP,
                                HSX = c.SanPham.HangSanXuat.TenHSX,
                                SoLuong = c.SoLuong,
                                DonGia = c.DonGia
                            };
                return query.ToList();
            }
        }

        public static int ThemCTHD (ChiTietHoaDon cthd)
        {
            using (QLSieuThiDTEntities ctx = new QLSieuThiDTEntities())
            {
                ctx.ChiTietHoaDons.Add(cthd);
                ctx.SaveChanges();
                return cthd.MaCTHD;
            }
        }

        public static void CapNhatSL (int macthd, int soluong)
        {
            using (QLSieuThiDTEntities ctx = new QLSieuThiDTEntities())
            {
                ChiTietHoaDon cthd = ctx.ChiTietHoaDons.FirstOrDefault(c => c.MaCTHD == macthd);
                cthd.SoLuong = soluong;
                ctx.SaveChanges();
            }
        }

        public static void XoaCTHD (int macthd)
        {
            using (QLSieuThiDTEntities ctx = new QLSieuThiDTEntities())
            {
                var del_cthd = new ChiTietHoaDon();
                del_cthd.MaCTHD = macthd;
                ctx.ChiTietHoaDons.Attach(del_cthd);
                ctx.ChiTietHoaDons.Remove(del_cthd);
                ctx.SaveChanges();
            }
        }
 
    }
}
