﻿using QLSTĐT_PTTKHTTT.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLSTĐT_PTTKHTTT.Xử_lý
{
    public static class HoaDonBUS
    {
        public static List<HoaDonDTO> LayDSHoaDon()
        {
            using (var ctx = new QLSieuThiDTEntities())
            {
                var query = from c in ctx.HoaDons
                            where c.TinhTrangHD.Equals("Đã thanh toán")
                            select new HoaDonDTO
                            {
                                MaHD = c.MaHD,
                                MaKH = c.MaTheKH,
                                TenCoupon = c.Coupon.TenCoupon,
                                NgayLapHD = c.NgayLapHD,
                                TongTienHang = c.TongTienHang,
                                HinhThucMuaHang = c.HinhThucMuaHang,
                                SoTienDuocGiam = c.SoTienDuocGiam,
                                TiLeGiamGia = c.TiLeGiamGia,
                                LoaiHoaDon = c.LoaiHoaDon,
                                TinhTrangHD = c.TinhTrangHD
                            };


                return query.ToList();
            }
        }

        public static int ThemHoaDon(HoaDon hd)
        {
            using (QLSieuThiDTEntities ctx = new QLSieuThiDTEntities())
            {
                ctx.HoaDons.Add(hd);
                ctx.SaveChanges();
                return hd.MaHD;
            }
        }

        public static int? TinhTienHang(int maHD)
        {
            int? TienHang = 0;
            using (QLSieuThiDTEntities ctx = new QLSieuThiDTEntities())
            {
                var query = ctx.HoaDons.Where(c => c.MaHD == maHD).FirstOrDefault();
                 TienHang = query.ChiTietHoaDons.Sum(c => c.SoLuong * c.DonGia);
                
                    }
            return TienHang;
        }

        public static HoaDon TimHoaDon(int maHD)
        {
            using (QLSieuThiDTEntities ctx = new QLSTĐT_PTTKHTTT.QLSieuThiDTEntities())
            {
                HoaDon hd = ctx.HoaDons.FirstOrDefault(c => c.MaHD == maHD);
                return hd;
            }
        }

        public static void CapNhatHoaDon(HoaDon hd)
        {
            using (QLSieuThiDTEntities ctx = new QLSieuThiDTEntities())
            {
                ctx.HoaDons.Attach(hd);
                ctx.Entry(hd).State = System.Data.Entity.EntityState.Modified;
                ctx.SaveChanges();
            }
        }
    }
}
