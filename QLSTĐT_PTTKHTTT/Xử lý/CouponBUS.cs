﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLSTĐT_PTTKHTTT.Xử_lý
{
   public static class CouponBUS
    {
        public static Coupon TimCoupon(int maCoupon)
        {
            using (QLSieuThiDTEntities ctx = new QLSieuThiDTEntities())
            {
                return ctx.Coupons.FirstOrDefault(c => c.MaCoupon == maCoupon);
            }
        }
    }
}
