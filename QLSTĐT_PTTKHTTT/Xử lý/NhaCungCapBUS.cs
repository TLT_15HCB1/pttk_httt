﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLSTĐT_PTTKHTTT.Xử_lý
{
    public static class NhaCungCapBUS
    {
        public static List<NCCDTO> LoadcbxNCC()
        {
            using (var ctx = new QLSieuThiDTEntities())
            {
                var query = from c in ctx.NhaCungCaps
                             select new NCCDTO
                             {
                                 MaNCC = c.MaNCC,
                                 TenNCC = c.TenNCC
                             };
                return query.ToList();
            }
        }
    }
}
