﻿using QLSTĐT_PTTKHTTT.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLSTĐT_PTTKHTTT.Xử_lý
{
    public static class ChiTietDonDatHangBUS
    {
        public static List<ChiTietDonDatHangDTO> LayDSChiTietDDH(int maDDH)
        {
            using (QLSieuThiDTEntities ctx = new QLSieuThiDTEntities())
            {
                var query = from c in ctx.ChiTietDonDatHangs
                            where c.MaDDH == maDDH
                            select new ChiTietDonDatHangDTO
                            {
                                MaCTDDH = c.MaCTDDH,
                                MaDDH = c.MaDDH,
                                MaSP = c.MaSP,
                                SoLuong = c.SoLuong,
                                DonGia = c.DonGia
                            };
                return query.ToList();
            }
        }

        public static int ThemCTDDH(ChiTietDonDatHang ctddh)
        {
            using (QLSieuThiDTEntities ctx = new QLSieuThiDTEntities())
            {
                ctx.ChiTietDonDatHangs.Add(ctddh);
                ctx.SaveChanges();
                return ctddh.MaCTDDH;
            }
        }

        public static int XoaCTDDH(int maDDH)
        {
            using (QLSieuThiDTEntities ctx = new QLSieuThiDTEntities())
            {
                ChiTietDonDatHang ctddh = ctx.ChiTietDonDatHangs.Find(maDDH);
                ctx.ChiTietDonDatHangs.Remove(ctddh);
                ctx.SaveChanges();
                return ctddh.MaCTDDH;
            }
        }

        public static int XacNhanDDH(int maDDH, int tongTien)
        {
            using (QLSieuThiDTEntities ctx = new QLSieuThiDTEntities())
            {
                DonDatHang ddh = ctx.DonDatHangs.Find(maDDH);
                ddh.TongTienHang = tongTien;
                ctx.SaveChanges();
                return ddh.MaDDH;
            }
        }

        public static void CapNhatSL(int maCTDDH, int soLuong)
        {
            using (QLSieuThiDTEntities ctx = new QLSieuThiDTEntities())
            {
                ChiTietDonDatHang cthd = ctx.ChiTietDonDatHangs.FirstOrDefault(c => c.MaCTDDH == maCTDDH);
                cthd.SoLuong = soLuong;
                ctx.SaveChanges();
            }
        }
    }
}
