﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLSTĐT_PTTKHTTT.Xử_lý
{
    public static class HangSanXuatBUS
    {
        public static List<HSXDTO> LoadcbxHSX()
        {
            using (var ctx = new QLSieuThiDTEntities())
            {
                var query = from c in ctx.HangSanXuats
                            select new HSXDTO
                            {
                                MaHSX = c.MaHSX,
                                TenHSX = c.TenHSX
                            };
                return query.ToList();
            }
        }

        //public static HangSanXuat LayHSX(int MaHSX)
        //{
            
        //    using (var ctx = new QLSieuThiDTEntities())
        //    {
        //        var hsx = ctx.HangSanXuats.Select(c => new { c.MaHSX, c.TenHSX }).Where(c => c.MaHSX == MaHSX).FirstOrDefault();
        //        return (HangSanXuat)hsx;
        //    }
        //}
    }
}
